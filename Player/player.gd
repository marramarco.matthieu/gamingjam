extends KinematicBody2D

# Declare member variables here. Examples:
var velocity
var delta_last_jump
var curr_anim
var new_anim
var sprite
var flip_h
var current_moon
var delta_landing
var death_timer

signal win
signal respawn

enum{JUMP, IDLE, FLOAT, RUN, DEAD}
var state

const h_speed = 30000
const v_speed = 75000
const gravity = 4000
const jump_boost_delta_min = 0.10
const jump_boost_delta_max = 0.40
const jump_cd = 0.1
const jump_boost = gravity / 1.7
const num_moon = 4

# Called when the node enters the scene tree for the first time.
func _ready():
	velocity = Vector2()
	delta_last_jump = 0.0
	sprite = get_node("Sprite")
	change_state(IDLE)
	flip_h = false
	current_moon = 0
	delta_landing = 0.0
	death_timer = 0.0
	
func rebirth(pos):
	position = pos
	velocity = Vector2()
	delta_last_jump = 0.0
	change_state(IDLE)
	flip_h = false
	sprite.visible = true
	death_timer = 0.0
	
func change_state(new_state):
	state = new_state
	match state:
		IDLE:
			new_anim = 'idle'
		RUN:
			new_anim = 'run'
		JUMP:
			new_anim = 'jump'
		FLOAT:
			new_anim = 'float'
		DEAD:
			sprite.visible = false
			for part in get_node("DeathParticles").get_children() :
				part.emitting = true

func getInputs(delta):
	velocity.x = 0
	var left = Input.is_action_pressed("pl_left")
	var right = Input.is_action_pressed("pl_right")
	var jump = Input.is_action_pressed("pl_jump")
	
	if jump and delta_last_jump < jump_boost_delta_max and delta_last_jump > jump_boost_delta_min and !is_on_floor():
		velocity.y -= jump_boost * delta
		
	if jump and is_on_floor() and delta_landing >= jump_cd:
		velocity.y = -v_speed * delta
		delta_last_jump = 0.0
		change_state(JUMP)
		
	if left:
		velocity.x -= h_speed * delta
		flip_h = true
		if is_on_floor():
			change_state(RUN)
			
	if right:
		velocity.x += h_speed * delta
		flip_h = false
		if is_on_floor():
			change_state(RUN)
	
	sprite.flip_h = flip_h
	
	if !right and !left and state==RUN:
		change_state(IDLE)
	
	if !is_on_floor():
		change_state(JUMP)
		if velocity.y > 0:
			change_state(FLOAT)

func _process(delta):
	if curr_anim != new_anim:
		curr_anim = new_anim
		sprite.play(new_anim)
	if(state == DEAD):
		death_timer += delta
		if(death_timer >= 2.0):
			emit_signal("respawn")

func _physics_process(delta):
	delta_last_jump += delta
	if is_on_floor():
		delta_landing += delta
	if state != DEAD and Constants.allow_player_move:
		getInputs(delta)
	velocity.y += gravity * delta
	
	if state == JUMP || state == FLOAT:
		if is_on_floor():
			change_state(IDLE)
			delta_landing = 0.0
	
	velocity = move_and_slide(velocity, Vector2(0, -1))
	for i in get_slide_count():
		var collision = get_slide_collision(i)
		if collision.collider.is_in_group("Trap"):
			change_state(DEAD)
		elif collision.collider.get_parent().has_method("sendCollected"):
			if(collision.collider.get_parent().getType() == Constants.Collectables.MOON):
				current_moon += 1
				if(current_moon == num_moon):
					emit_signal("win")
			collision.collider.get_parent().sendCollected()