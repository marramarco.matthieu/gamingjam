extends Node

# Declare member variables here. Examples:
var current_button
var is_help_opened

var play_p = preload("Data/Carton_Boutton_Play_Valide.png")
var help_p = preload("Data/Carton_Boutton_Options_Valide.png")
var quit_p = preload("Data/Carton_Boutton_Quit_Valide.png")
var play = preload("Data/Carton_Boutton_Play.png")
var help = preload("Data/Carton_Boutton_Options.png")
var quit = preload("Data/Carton_Boutton_Quit.png")

# Called when the node enters the scene tree for the first time.
func _ready():
	current_button = 0
	is_help_opened = false
	
func press_button(id):
	if id == 0:
		get_tree().change_scene("res://Scenes/Main.tscn")
	elif id == 1:
		is_help_opened = true
		get_node("helpboard").visible = true
	else:
		get_tree().quit()

func hover_button(id):
	if id == 0:
		get_node("play").texture = play_p
	elif id == 1:
		get_node("help").texture = help_p
	else:
		get_node("quit").texture = quit_p

func leave_button(id):
	if id == 0:
		get_node("play").texture = play
	elif id == 1:
		get_node("help").texture = help
	else:
		get_node("quit").texture = quit

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	var up = Input.is_action_just_pressed("pl_up")
	var down = Input.is_action_just_pressed("pl_down")
	var press = Input.is_action_just_pressed("pl_jump")
	if (up or down) and !is_help_opened:
		leave_button(current_button)
		current_button =  (current_button - int(up) + int(down) + 3) % 3
		hover_button(current_button)
		
	if press:
		if is_help_opened :
			is_help_opened = false 
			get_node("helpboard").visible = false
		else:
			press_button(current_button)
