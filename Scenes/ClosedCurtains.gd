extends TextureRect

var previous_color

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

func gain_focus():
	previous_color = modulate
	modulate = Color(1,0,0,1)

func lose_focus():
	if(previous_color != null):
		modulate = previous_color

