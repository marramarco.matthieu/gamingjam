extends Node2D

export var ODD_SIZE_MAP:int = 5

const COL_OFFSET = 1536 #24*64
const ROW_OFFSET = 2048 #32*64

var map = []
var focus_position
var VeilAnimation:AnimationPlayer
var StartScene:Node2D
var SelectScene
var scene_to_build
var state

var spawn_point:Vector2
var zoom_point:Vector2

enum GAME_STATE {PLAYING, SELECTING, BUILDING}

func _ready():
	
	#initialze random
	randomize()
	
	if(ODD_SIZE_MAP%2 == 0):
		ODD_SIZE_MAP += 1
	
	#build the closed curtains panel, and store it into the map
	var closed_curtain_packed_scene = load("res://Scenes/ClosedCurtainsScene.tscn")
	var closed_curtain_node
	for row in range(ODD_SIZE_MAP):
		map.append([])
		for col in range(ODD_SIZE_MAP):
			closed_curtain_node = closed_curtain_packed_scene.instance()
			closed_curtain_node.rect_scale = Vector2(ROW_OFFSET/2480.0,COL_OFFSET/1772.0)
			closed_curtain_node.rect_position.x =row * ROW_OFFSET
			closed_curtain_node.rect_position.y = col * COL_OFFSET
			map[row].append(closed_curtain_node)
			add_child(closed_curtain_node)
	
	#we remove the middle scene, and replace it with the starting scene
	RemoveScene(ODD_SIZE_MAP/2,ODD_SIZE_MAP/2)
	
	var first_scene_row = ODD_SIZE_MAP/2 * ROW_OFFSET
	var first_scene_col = ODD_SIZE_MAP/2 * COL_OFFSET
	StartScene = get_node("FirstScene")
	StartScene.position.x = first_scene_row
	StartScene.position.y = first_scene_col
	map[ODD_SIZE_MAP/2][ODD_SIZE_MAP/2] = StartScene
	
	zoom_point.x = StartScene.position.x + COL_OFFSET / 2
	zoom_point.y = StartScene.position.y + ROW_OFFSET / 2
	
	var x_player_offset = 300
	var y_player_offset = 300
	$Player.position.x = x_player_offset + first_scene_row
	$Player.position.y = y_player_offset + first_scene_col
	spawn_point = $Player.position
	
	SelectScene = get_node("CanvasLayer/HSplitContainer")
	VeilAnimation = get_node("Veil/VeilAnimation")
	
	change_state(GAME_STATE.PLAYING)
	Constants.allow_player_move = true
	
	SelectScene.connect("chose_scene", self, "on_scene_chosen_received")
	StartScene.get_node("Collectable").connect("collected", self, "on_collected_received")
	$Player.connect("win", self, "win_event")

func change_state(new_state):
	state = new_state
	match state:
		GAME_STATE.SELECTING:
			Constants.allow_player_move = false
			startSelecting()
			Constants.is_selecting = true
			var arr = get_tree().get_nodes_in_group("buildable")
			print(arr.size())

func _process(delta):	
	if(state == GAME_STATE.BUILDING):
		decideWhereTobuild()

func startSelecting():
	var player:KinematicBody2D = get_node("Player")
	var camera = player.get_node("Camera")
	player.position = zoom_point
	camera.zoom = Vector2(ODD_SIZE_MAP*2,ODD_SIZE_MAP*2)
	VeilAnimation.play("darken")
	$CanvasLayer/HSplitContainer.selectLevels()
	SelectScene.show()

func buildNewScene():
	print("Position de la scène créée : " + str(focus_position.x) + ", " + str(focus_position.y))
	RemoveScene(focus_position.x, focus_position.y)
	var scene = scene_to_build.instance()
	scene.add_to_group("instanciated")
	scene.position.x = focus_position.x * ROW_OFFSET
	scene.position.y = focus_position.y * COL_OFFSET
	add_child(scene)
	removeNeighboringDoors(scene)
	if(scene_is_at_edges()):
		scene.get_node("Collectable").setType(Constants.Collectables.MOON)
	scene.get_node("Collectable").connect("collected", self, "on_collected_received")
	map[focus_position.x][focus_position.y] = scene
	VeilAnimation.play("brighten")
	focus_position = null
	get_node("Timer").start()
	scene.get_node("AnimatedSprite").play("curtain_up")


func removeNeighboringDoors(scene):
	var scene_left
	if(focus_position.x-1>=0):
		scene_left = map[focus_position.x-1][focus_position.y]
	
	var scene_right 
	if(focus_position.x+1 < ODD_SIZE_MAP):
		scene_right = map[focus_position.x+1][focus_position.y]
	
	var scene_up 
	if(focus_position.y-1>=0):
		scene_up = map[focus_position.x][focus_position.y-1]
	
	var scene_down
	if(focus_position.y+1<ODD_SIZE_MAP):
		scene_down = map[focus_position.x][focus_position.y+1]
	
	if(scene_left != null and scene_left.is_in_group("instanciated") and scene_left.right_exit and scene.left_exit):
		scene_left.remove_right()
		scene.remove_left()
	if(scene_right != null and scene_right.is_in_group("instanciated") and scene_right.left_exit and scene.right_exit):
		scene.remove_right()
		scene_right.remove_left()
	if(scene_up != null and scene_up.is_in_group("instanciated") and scene_up.down_exit and scene.up_exit):
		scene_up.remove_down()
		scene.remove_up()
	if(scene_down != null and scene_down.is_in_group("instanciated") and scene_down.up_exit and scene.down_exit):
		scene_down.remove_up()
		scene.remove_down()

func scene_is_at_edges():
	if(focus_position.x == 0 and focus_position.y == 0 or
		focus_position.x == ODD_SIZE_MAP-1 and focus_position.y == 0 or
		focus_position.x == 0 and focus_position.y == ODD_SIZE_MAP-1 or
		focus_position.x == ODD_SIZE_MAP-1 and focus_position.y == ODD_SIZE_MAP-1):
			return true
	return false

func CheckForBuildability():
	var this_scene = scene_to_build.instance()
	
	var scene_left
	if(focus_position.x-1>=0):
		scene_left = map[focus_position.x-1][focus_position.y]
	
	var scene_right 
	if(focus_position.x+1 < ODD_SIZE_MAP):
		scene_right = map[focus_position.x+1][focus_position.y]
	
	var scene_up 
	if(focus_position.y-1>=0):
		scene_up = map[focus_position.x][focus_position.y-1]
	
	var scene_down 
	if(focus_position.y+1<ODD_SIZE_MAP):
		scene_down = map[focus_position.x][focus_position.y+1]
	
	if(scene_left != null and scene_left.is_in_group("instanciated") and scene_left.right_exit and this_scene.left_exit or 
	scene_right != null and scene_right.is_in_group("instanciated") and scene_right.left_exit and this_scene.right_exit or
	scene_up != null and scene_up.is_in_group("instanciated") and scene_up.down_exit and this_scene.up_exit or
	scene_down != null and scene_down.is_in_group("instanciated") and scene_down.up_exit and this_scene.down_exit):
		print("buildable")
		return true
	
	return false

func decideWhereTobuild():
	if(focus_position == null):
		SetFocusPosition()
	if(Input.is_action_just_pressed("pl_down")):
		var move_down = find_new_focus_down()
		if(move_down != -1):
			map[focus_position.x][focus_position.y].lose_focus()
			focus_position.y += move_down
			map[focus_position.x][focus_position.y].gain_focus()
	if(Input.is_action_just_pressed("pl_up")):
		var move_up = find_new_focus_up()
		if(move_up != -1):
			map[focus_position.x][focus_position.y].lose_focus()
			focus_position.y -= move_up
			map[focus_position.x][focus_position.y].gain_focus()
	if(Input.is_action_just_pressed("pl_right")):
		var move_right = find_new_focus_right()
		if(move_right != -1):
			map[focus_position.x][focus_position.y].lose_focus()
			focus_position.x += move_right
			map[focus_position.x][focus_position.y].gain_focus()
	if(Input.is_action_just_pressed("pl_left")):
		var move_left = find_new_focus_left()
		if(move_left != -1):
			map[focus_position.x][focus_position.y].lose_focus()
			focus_position.x -= move_left
			map[focus_position.x][focus_position.y].gain_focus()
	if(Input.is_action_just_pressed("pl_jump")):
		map[focus_position.x][focus_position.y].lose_focus()
		Constants.is_selecting = false
		if CheckForBuildability():
			buildNewScene()
			change_state(GAME_STATE.PLAYING)
			Constants.allow_player_move = true
			$Player.rebirth(spawn_point)

#remove the scene located at var x, var y
func RemoveScene(var x, var y):
	map[x][y].queue_free()
	map[x][y] = null

func SetFocusPosition():
	var x = 0
	var y = 0
	var loop = true
	
	while x < ODD_SIZE_MAP and loop:
		while y < ODD_SIZE_MAP and loop:
			if(map[x][y].is_in_group("buildable")):
				loop = false
				focus_position = Vector2(x,y)
				map[x][y].gain_focus()
				print("gained focus on " + str(x) + " "+ str(y))
			y += 1
		x += 1
		y = 0

func _on_VeilAnimation_animation_finished(anim_name):
	if(anim_name == "darken"):
		$'Veil'.modulate.a = 0.39
	else:
		$'Veil'.modulate.a = 0

func find_new_focus_down() -> int:
	var i = 1
	while focus_position.y+i < ODD_SIZE_MAP:
		if(map[focus_position.x][focus_position.y+i].is_in_group("buildable")):
			return i
		i += 1
	return -1

func find_new_focus_up() -> int:
	var i = 1
	while focus_position.y-i >= 0:
		if(map[focus_position.x][focus_position.y-i].is_in_group("buildable")):
			return i
		i += 1
	return -1

func find_new_focus_right() -> int:
	var i = 1
	while focus_position.x+i < ODD_SIZE_MAP:
		if(map[focus_position.x+i][focus_position.y].is_in_group("buildable")):
			return i
		i += 1
	return -1

func find_new_focus_left() -> int:
	var i = 1
	while focus_position.x-i >= 0:
		if(map[focus_position.x-i][focus_position.y].is_in_group("buildable")):
			return i
		i += 1
	return -1

func on_scene_chosen_received(path):
	print("chose left scene!")
	SetFocusPosition()
	change_state(GAME_STATE.BUILDING)
	scene_to_build = load(path)

func on_collected_received(var type):
	if(type == Constants.Collectables.MOON): #reset cp
		spawn_point = $Player.position
	change_state(GAME_STATE.SELECTING)

func win_event():
	get_tree().change_scene("res://MainMenu/mainmenu.tscn")


func _on_Player_respawn():
	$Player.rebirth(spawn_point)


func _on_Timer_timeout():
	var camera = get_node("Player").get_node("Camera")
	camera.zoom = Vector2(2,2)
