extends HBoxContainer

signal chose_scene
var send_left = true
var path_scene_left
var path_scene_right

# Called when the node enters the scene tree for the first time.
func _ready():
	$VBoxContainer/LeftChoice.material.set_shader_param("width",8)
	selectLevels()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	if(Constants.is_selecting):
		if(Input.is_action_just_pressed("pl_left")):
			$VBoxContainer/LeftChoice.material.set_shader_param("width",8)
			$VBoxContainer2/RightChoice.material.set_shader_param("width",0)
			send_left = true;
		if(Input.is_action_just_pressed("pl_right")):
			$VBoxContainer/LeftChoice.material.set_shader_param("width",0)
			$VBoxContainer2/RightChoice.material.set_shader_param("width",8)
			send_left = false
		if(Input.is_action_just_pressed("pl_jump")):
			hide()
			if(send_left):
				emit_signal("chose_scene", path_scene_left)
			else:
				emit_signal("chose_scene", path_scene_right)

func selectLevels():
	get_node("VBoxContainer2/RightChoice/Fleche").hide()
	get_node("VBoxContainer2/RightChoice/Fleche2").hide()
	get_node("VBoxContainer2/RightChoice/Fleche3").hide()
	get_node("VBoxContainer2/RightChoice/Fleche4").hide()
	get_node("VBoxContainer2/RightChoice/Fleche5").hide()
	get_node("VBoxContainer2/RightChoice/Fleche6").hide()
	get_node("VBoxContainer2/RightChoice/Fleche7").hide()
	get_node("VBoxContainer2/RightChoice/Fleche8").hide()
	
	path_scene_left = "res://Scenes/Levels/"+str((randi()%Constants.nb_levels)+1)+"_Scene.tscn"
	path_scene_right = "res://Scenes/Levels/"+str((randi()%Constants.nb_levels)+1)+"_Scene.tscn"
	
	var sc_left = load(path_scene_left).instance()
	var sc_right = load(path_scene_right).instance()
	
	if sc_left.up_exit:
		get_node("VBoxContainer2/RightChoice/Fleche").show()
	if sc_left.right_exit:
		get_node("VBoxContainer2/RightChoice/Fleche2").show()
	if sc_left.down_exit:
		get_node("VBoxContainer2/RightChoice/Fleche3").show()
	if sc_left.left_exit:
		get_node("VBoxContainer2/RightChoice/Fleche4").show()
	
	if sc_right.up_exit:
		get_node("VBoxContainer2/RightChoice/Fleche5").show()
	if sc_right.right_exit:
		get_node("VBoxContainer2/RightChoice/Fleche6").show()
	if sc_right.down_exit:
		get_node("VBoxContainer2/RightChoice/Fleche7").show()
	if sc_right.left_exit:
		get_node("VBoxContainer2/RightChoice/Fleche8").show()
	
	print(path_scene_left)
	print(path_scene_right)
