extends Node2D

# Declare member variables here. Examples:
# var a = 2
# var b = "text"

export(Vector2) var p1
export(Vector2) var p2
export(float) var speed = 500

var way = 1;

onready var follow = get_node("Path2D/PathFollow2D")

# Called when the node enters the scene tree for the first time.
func _ready():
	self.position = p1
	var cu = Curve2D.new()
	cu.add_point(p1)
	cu.add_point(p1 + (p2-p1)/scale)
	get_node("Path2D").curve = cu
	get_node("Path2D/PathFollow2D/AnimationPlayer").play("turn")
	pass # Replace with function body.

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	follow.offset +=  way * speed * delta
	if follow.unit_offset >= 1.0:
		way *= -1
	if follow.unit_offset <= 0.0:
		way *= -1
