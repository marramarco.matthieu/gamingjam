extends Sprite

signal collected

var spriteStar = preload("res://assets/Star_1.png")
var spriteMoon = preload("res://assets/Moon.png")
onready var an_player = get_node("AnimationPlayer")
var collected

export(Constants.Collectables) var type

func _ready():
	setType(type)
	collected = false


func getType():
	return type

func setType(newType):
	type = newType
	match type:
		Constants.Collectables.MOON:
			self.texture = spriteMoon
		Constants.Collectables.STAR:
			self.texture = spriteStar

func sendCollected():
	if !collected :
		collected = true
		get_node("StaticBody2D/CollisionShape2D").disabled = true
		an_player.play("get_collectable")
		get_node("Jingle").play()

func _on_AnimationPlayer_animation_finished(anim_name, extra_arg_0):
	emit_signal("collected", type)
