extends Node2D

export var left_exit:bool
export var right_exit:bool
export var down_exit:bool
export var up_exit:bool

const height = 1536 #24*64
const width = 2048 #32*64

var door_left
var door_right
var door_down
var door_up

# Called when the node enters the scene tree for the first time.
func _ready():
	put_door()

func put_door():
	var door_packed_scene = load("res://Scenes/Entities/closeDoor.tscn")
	if(left_exit):
		door_left = door_packed_scene.instance()
		door_left.position = Vector2(0, height/2)
		add_child(door_left)
	if(right_exit):
		door_right = door_packed_scene.instance()
		door_right.position = Vector2(width, height/2)
		add_child(door_right)
	if(down_exit):
		door_down = door_packed_scene.instance()
		door_down.position = Vector2(width/2, height)
		door_down.rotation_degrees = 90
		add_child(door_down)
	if(up_exit):
		door_up = door_packed_scene.instance()
		door_up.position = Vector2(width/2, 0)
		door_up.rotation_degrees = 90
		add_child(door_up)

func remove_up():
	door_up.queue_free()

func remove_down():
	door_down.queue_free()

func remove_right():
	door_right.queue_free()

func remove_left():
	door_left.queue_free()